﻿/********************************************************************
 * Copyright (C)   Huawei Tech. Co., Ltd.
 * 文 件 名    :   DmpApi.h
 * 文件描述    :   DMPBASE对外接口头文件
 ********************************************************************/

// 经常有人把DmpPub.h和DmpApi.h顺序搞反，我来兼容处理一下算了
#ifndef __DMPPUB__
#include "DmpPub.h"
#endif

#ifndef __DMPAPI__
#define __DMPAPI__

/********************************************************************

                        DMP平台日志使用说明
一、简介
1、日志级别的定义
    DMP平台定义了DEBUG/INFO/WARNNING/ERROR四个级别，含义如下:
    DMP_LOG_DEBUG : 调试级别，仅供开发人员调试用
    DMP_LOG_INFO  : 信息级别，记录运行过程
    DMP_LOG_WARN  : 告警级别，不影响业务的一般错误
    DMP_LOG_ERROR : 错误级别，影响业务的严重错误

2、日志的输出
    各平台下统一用下面四个宏定义来输出日志，单条最长4096字节
    DMP_LOG_DEBUG(tag, fmt, ...)
    DMP_LOG_INFO(tag, fmt, ...)
    DMP_LOG_WARN(tag, fmt, ...)
    DMP_LOG_ERROR(tag, fmt, ...)

3、日志标签
    日志标签(tag)用于区分模块，便于日志筛选，安卓下的LogCat日志原生支持。
    建议使用DMP日志模块的应用统一使用大小写的格式设计标签，例如"DmpLog"。

4、日志模块的初始化
    日志是DMP平台库的一个模块，各平台下的初始化方法见下面的开发指南。

二、各平台开发指南
1、iOS平台
    (1) 开/关NSLog日志
    默认情况下日志模块是没有任何输出的，每个日志通道都必须单独打开。
    NSLog日志通道的默认输出级别是INFO级别。
    iOS系统提供的NSLog输出通道打开/关闭接口如下，详见相关函数注释:
    DmpOpenIosNSLog/DmpCloseIosNSLog

    (2) 开/关本地文件日志
    打开本地文件日志的时候必须指定一个可写路径，例如"<Documents>/Logs"。
    如果路径不存在，日志模块会自动创建。
    日志模块会在指定的路径下每次打开的时候创建一个日志文件，直到日志
通道被关闭的时候才会关闭。日志模块会自动保留最近的10个日志文件。
    本地文件日志通道的默认输出级别是DEBUG级别。
    本地文件日志通道打开/关闭接口如下，详见相关函数注释:
    DmpOpenLocalFileLog/DmpCloseLocalFileLog

    (3) 设置日志级别
    每个日志通道的默认输出级别都各不相同，但可以通过接口进行动态修改。
    日志输出级别设置接口如下，详见相关函数注释:
    DmpSetLogLevel

2、Android平台
    (1) LogCat日志
    默认情况下日志模块是没有任何输出的，每个日志通道都必须单独打开。
    LogCat日志通道的默认输出级别是DEBUG级别。
    Android系统提供的LogCat输出通道打开/关闭接口如下，详见相关java接口文档。
    DmpLog::OpenLogCatLog/DmpLog::CloseLogCatLog

    (2) 开/关本地文件日志
    打开本地文件日志的时候必须指定一个已存在的可写路径，例如"/sdcard/logs"。
    日志模块会在指定的路径下每次打开的时候创建一个日志文件，直到日志
通道被关闭的时候才会关闭。日志模块会自动保留最近的10个日志文件。
    本地文件日志通道的默认输出级别是DEBUG级别。
    本地文件日志通道打开/关闭接口如下，详见相关java接口文档。
    DmpLog::OpenLocalFileLog/DmpLog::CloseLocalFileLog

    (3) 设置日志级别
    每个日志通道的默认输出级别都各不相同，但可以通过接口进行动态修改。
    日志输出级别设置接口如下，详见相关函数注释:
    DmpLog::SetLogLevel

*********************************************************************/

#ifdef    __cplusplus
extern "C" {
#endif


/********************************************************************
* 函 数 名:  InitDmpBase
* 描    述:  DMP平台初始化
* 输入参数:  无
* 输出参数:  无
* 返 回 值:  DMP_OK: 成功，其他值: 失败
* 说    明:  此接口无需应用层调用，仅为兼容性保留
********************************************************************/
DMP_API INT32 InitDmpBase();


// 日志级别定义
typedef enum
{
    DMP_LOG_DEBUG = 0,      // 调试级别，仅供开发人员调试用
    DMP_LOG_INFO,           // 信息级别，记录运行过程
    DMP_LOG_WARN,           // 告警级别，不影响业务的一般错误
    DMP_LOG_ERROR,          // 错误级别，影响业务的严重错误

    DMP_LOG_DISABLED = 10   // 日志输出被禁用
} DMP_LOG_LEVEL_E;


/********************************************************************
 * 函 数 名:  DmpLog
 * 描    述:  日志接口
 * 输入参数:  level         日志级别
              tag           日志标签，可以使用模块名称，例如"EppProxy"
              file          调用者的文件名，由宏定义自动传入
              line          调用者的行号，由宏定义自动传入
              fmt           格式化字符串
 * 输出参数:  无
 * 返 回 值:  无
 * 使用建议:  1. 日志内容首字母大写
              2. 结尾有英文半角句号
              3. 尾部不必添加回车符
              4. 日志标签中不要出现特殊字符
 ********************************************************************/
DMP_API VOID DmpLog(DMP_LOG_LEVEL_E level,
                    const CHAR *tag,
                    const CHAR *file,
                    INT32 line,
                    const CHAR *fmt, ...);


/********************************************************************
 * 函 数 名:  DmpOsLog
 * 描    述:  操作系统日志接口，不受日志开关控制
 * 输入参数:  level         日志级别
              tag           日志标签，可以使用模块名称，例如"EppProxy"
              file          调用者的文件名，由宏定义自动传入
              line          调用者的行号，由宏定义自动传入
              fmt           格式化字符串
 * 输出参数:  无
 * 返 回 值:  无
 * 使用建议:  1. 日志内容首字母大写
              2. 结尾有英文半角句号
              3. 尾部不必添加回车符
              4. 日志标签中不要出现特殊字符
 ********************************************************************/
DMP_API VOID DmpOsLog(DMP_LOG_LEVEL_E level,
                      const CHAR *tag,
                      const CHAR *file,
                      INT32 line,
                      const CHAR *fmt, ...);

/********************************************************************
 * 函 数 名:  DmpIosLog
 * 描    述:  iOS日志接口，支持NSObject对象格式化
 * 输入参数:  level         日志级别
              tag           日志标签，可以使用模块名称，例如"EppProxy"
              file          调用者的文件名，由宏定义自动传入
              line          调用者的行号，由宏定义自动传入
              fmt           格式化字符串
 * 输出参数:  无
 * 返 回 值:  无
 * 使用建议:  1. 日志内容首字母大写
              2. 结尾有英文半角句号
              3. 尾部不必添加回车符
              4. 日志标签中不要出现特殊字符
 ********************************************************************/
#if __OBJC__
DMP_API VOID DmpNsLog(DMP_LOG_LEVEL_E level,
                      NSString *tag,
                      const CHAR *file,
                      INT32 line,
                      NSString *fmt, ...);
#endif

// 应用层调用日志接口宏定义
#if __OBJC__
	#define DMP_LOG_DEBUG(tag, fmt, arg ...)    DmpNsLog(DMP_LOG_DEBUG, tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_INFO(tag, fmt, arg ...)     DmpNsLog(DMP_LOG_INFO,  tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_WARN(tag, fmt, arg ...)     DmpNsLog(DMP_LOG_WARN,  tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_ERROR(tag, fmt, arg ...)    DmpNsLog(DMP_LOG_ERROR, tag, __FILE__, __LINE__, fmt, ##arg)
#else
	#define DMP_LOG_DEBUG(tag, fmt, arg ...)    DmpLog(DMP_LOG_DEBUG, tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_INFO(tag, fmt, arg ...)     DmpLog(DMP_LOG_INFO,  tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_WARN(tag, fmt, arg ...)     DmpLog(DMP_LOG_WARN,  tag, __FILE__, __LINE__, fmt, ##arg)
	#define DMP_LOG_ERROR(tag, fmt, arg ...)    DmpLog(DMP_LOG_ERROR, tag, __FILE__, __LINE__, fmt, ##arg)
#endif

// 应用层调用系统日志接口宏定义
#define DMP_OS_LOG_DEBUG(tag, fmt, arg ...)    DmpOsLog(DMP_LOG_DEBUG, tag, __FILE__, __LINE__, fmt, ##arg)
#define DMP_OS_LOG_INFO(tag, fmt, arg ...)     DmpOsLog(DMP_LOG_INFO,  tag, __FILE__, __LINE__, fmt, ##arg)
#define DMP_OS_LOG_WARN(tag, fmt, arg ...)     DmpOsLog(DMP_LOG_WARN,  tag, __FILE__, __LINE__, fmt, ##arg)
#define DMP_OS_LOG_ERROR(tag, fmt, arg ...)    DmpOsLog(DMP_LOG_ERROR, tag, __FILE__, __LINE__, fmt, ##arg)


#ifdef __APPLE__
/********************************************************************
 * 函 数 名:  DmpOpenIosNSLog
 * 描    述:  iOS下打开NSLog日志通道
 * 输入参数:  level         日志级别
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpOpenIosNSLog(DMP_LOG_LEVEL_E level);

/********************************************************************
 * 函 数 名:  DmpSetIosNSLogLevel
 * 描    述:  iOS下设置NSLog日志通道级别
 * 输入参数:  level         日志级别
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpSetIosNSLogLevel(DMP_LOG_LEVEL_E level);

/********************************************************************
 * 函 数 名:  DmpCloseIosNSLog
 * 描    述:  iOS下关闭NSLog日志通道
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpCloseIosNSLog();

/********************************************************************
 * 函 数 名:  DmpIosEnableCrashReport
 * 描    述:  允许捕获iOS下的异常并输出崩溃日志
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpIosEnableCrashReport(VOID);

/********************************************************************
 * 函 数 名:  DmpIosDisableCrashReport
 * 描    述:  禁止捕获iOS下的异常
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpIosDisableCrashReport(VOID);

/********************************************************************
 * 函 数 名:  DmpIosIsJailBroken
 * 描    述:  检测iOS设备是否已越狱
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  TRUE: 已越狱， FALSE: 未越狱
 ********************************************************************/
DMP_API BOOL DmpIosIsJailBroken();

#endif

/********************************************************************
 * 函 数 名:  DmpOpenLocalFileLog
 * 描    述:  打开本地文件日志通道，支持所有OS平台
 * 输入参数:  log_path      日志输出路径
              level         日志级别
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpOpenLocalFileLog(const CHAR *log_path, DMP_LOG_LEVEL_E level);


/********************************************************************
 * 函 数 名:  DmpSetLocalFileLogLevel
 * 描    述:  设置本地文件日志通道级别
 * 输入参数:  level         日志级别
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpSetLocalFileLogLevel(DMP_LOG_LEVEL_E level);

/********************************************************************
 * 函 数 名:  DmpCloseLocalFileLog
 * 描    述:  关闭本地文件日志通道
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpCloseLocalFileLog();

/********************************************************************
 * 函 数 名:  DmpSetDomainList
 * 描    述:  设置预解析域名列表
 * 输入参数:  domain_list        域名列表,'|'分隔，最多12个
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_C_API VOID DmpSetDomainList(const CHAR *domain_list);


// 系统内存信息
typedef struct _DMP_MEM_INFO_S
{
    UINT32 total_phy;           // 可用内核内存大小，单位KB
    UINT32 free_phy;            // 空闲内核内存大小，单位KB

    UINT32 resident_size;       // 常驻物理内存大小，单位KB
    UINT32 virtual_size;        // 进程虚拟内存大小，单位KB
} DMP_MEM_INFO_S;

/********************************************************************
 * 函 数 名:  DmpSysGetMemInfo()
 * 描    述:  获取系统内存信息
 * 输入参数:  无
 * 输出参数:  mem_info      内存信息
 * 返 回 值:  DMP_OK        成功
 *            其他值        失败
 ********************************************************************/
DMP_C_API INT32 DmpSysGetMemInfo(DMP_MEM_INFO_S *mem_info);

/********************************************************************
 * 函 数 名:  DmpSysGetCpuUsage()
 * 描    述:  获取CPU占用率
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  CPU占用率，0~100
 ********************************************************************/
DMP_C_API UINT32 DmpSysGetCpuUsage();

/********************************************************************
 * 函 数 名:  DmpMalloc
 * 描    述:  申请内存
 * 输入参数:  size          申请长度
 * 输出参数:  无
 * 返 回 值:  内存指针，NULL表示失败
 * 说    明:  自定义内存申请/释放接口的主要原因是Windows下MFC主程序
              里面分配的内存在NONE-MFC的DLL代码里面释放会崩溃
 ********************************************************************/
DMP_C_API VOID *DmpMalloc(UINT32 size);

/********************************************************************
 * 函 数 名:  DmpFree
 * 描    述:  释放内存
 * 输入参数:  ptr           待释放指针
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_C_API VOID DmpFree(VOID * ptr);


// 崩溃日志文件列表缓冲区大小
#define DMP_CRASH_LIST_BUF_SIZE 300

/********************************************************************
 * 函 数 名:  DmpGetCrashList
 * 描    述:  获取崩溃日志文件列表
 * 输入参数:  无
 * 输出参数:  buffer        崩溃日志文件列表缓冲区，逗号分隔
              size          buffer大小，单位字节
 * 返 回 值:  崩溃日志文件个数
 * 注    意:  应用层应保证buffer的大小至少为DMP_CRASH_LIST_BUF_SIZE
 ********************************************************************/
DMP_C_API INT32 DmpGetCrashList(CHAR *buffer, UINT32 size);

/********************************************************************
 * 函 数 名:  DmpGetCrashTime
 * 描    述:  获取崩溃发生时间
 * 输入参数:  crash_file    崩溃日志文件名
 * 输出参数:  无
 * 返 回 值:  1970年到现在的毫秒数
 ********************************************************************/
DMP_C_API UINT64 DmpGetCrashTime(const CHAR *crash_file);

/********************************************************************
 * 函 数 名:  DmpGetCrashCause
 * 描    述:  获取崩溃发生原因
 * 输入参数:  crash_file    崩溃日志文件名
 * 输出参数:  无
 * 返 回 值:  NULL          获取失败
              其他值        崩溃原因说明，用完后必须使用DmpFree释放
 ********************************************************************/
DMP_C_API CHAR * DmpGetCrashCause(const CHAR *crash_file);

/********************************************************************
 * 函 数 名:  DmpGetCrashBootTime
 * 描    述:  获取崩溃发生时应用已启动时间
 * 输入参数:  crash_file    崩溃日志文件名
 * 输出参数:  无
 * 返 回 值:  崩溃发生时应用已启动时间，单位毫秒
 * 注    意:  用DmpGetCrashTime()获取的崩溃时间减去此函数得到的崩溃时
              系统已启动时间，即可得到系统启动的1970年以来的毫秒数
 ********************************************************************/
DMP_C_API UINT64 DmpGetCrashBootTime(const CHAR *crash_file);

/********************************************************************
 * 函 数 名:  DmpGetCrashReport
 * 描    述:  获取崩溃日志内容
 * 输入参数:  crash_file    崩溃日志文件名
 * 输出参数:  无
 * 返 回 值:  NULL          获取失败
              其他值        崩溃日志内容，用完后必须使用DmpFree释放
 ********************************************************************/
DMP_C_API CHAR * DmpGetCrashReport(const CHAR *crash_file);


#ifdef __cplusplus
}
#endif


#endif // __DMPAPI__

