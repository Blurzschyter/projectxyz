/***************************************************************************

project :  (E)nhanced  (O)ffline   (P)layer


 _______   ____   ____
 | |_____ /  _ \ |  _ \
 |  \___||  (_) \| |_) |
 | |___  \      /|  _ /
 \_____|  \_ _ / |_|


****************************************************************************/
/*
 * @file                EopPubdefine.h
 * @brief               eop extern public definition
 * @date                2014/11/14
 * @version 1.0         Copyright 2014 huawei. All rights reserved.
 */

#ifndef __EOP_PUB_DEFINE_H
#define __EOP_PUB_DEFINE_H

#ifdef LIBEOP_EXPORTS
#define EXPORT_C  __declspec(dllexport)
#else
#define EXPORT_C
#endif

// dll版本号
#define EOP_VERSION_MAJOR 20
#define EOP_VERSION_MINOR 0
#define EOP_VERSION_PATCH 21
#define EOP_VERSION_BUILD 30

#define  EOP_VERSION "Eop.Base.20.0.21.30"

#define  EOP_TAG "EOP_OCM"

//定义下载任务的状态|Define the status of the download task
typedef enum EOP_DOWNLOAD_STATE_OPT_E
{
    EOP_DOWNLOAD_START = 0,//下载开始
    EOP_DOWNLOAD_PAUSE,    //下载暂停
    EOP_DOWNLOAD_WAITING,  //下载等待
    EOP_DOWNLOAD_FINISHED, //下载完成
    EOP_DOWNLOAD_FAILED,   //下载失败
    EOP_DOWNLOAD_DELETING, //正在删除
}EOP_DOWNLOAD_STATE_OPT;

typedef enum EOP_OPT
{
	//分段思想进行|Segmented thinking
	//1-100   下载模块使用|Download module use
	EOP_DOWNLOAD_SPEED = 1,                 //double输入|double input
	EOP_DOWNLOAD_SIZE,                      //int64 输入|int64 input
	EOP_DOWNLOAD_BITRATE_NO_USE,            //double 输入|double input
	EOP_DOWNLOAD_STAREM_DURATION,           // 暂时不用，保留|Not use temporarily, keep
	EOP_DOWNLOAD_TASK_LIST,                 //暂时不用，保留|Not use temporarily, keep
	EOP_DOWNLOAD_SPEED_LIMIT,              //double 类型传入|double type passed in
	EOP_DOWNLOAD_PROGRESS,                 //int32 类型传入|int32 type passed in
    EOP_DOWNLOAD_FINISHED_SIZE,            //int64 类型传入, 获取已下载完成的大小，返回单位 bytes|Int64 type is passed in, get the downloaded size, and the unit of bytes is returned
    EOP_DOWNLOAD_SOURCE_SIZE,              // int64 类型传入,获取估算出的片源的总大小,返回的单位为字节 (bytes)|The int64 type is passed in to obtain the estimated total size of the source. The returned unit is bytes (bytes)
    EOP_DOWNLOAD_FINISHED_PLAY_TIME,       // float类型传入，获取已缓存的片源可以播放的时长 ,返回单位为秒|Float type is passed in, get the duration that the cached film source can play, the return unit is second
    EOP_DOWNLOAD_BITRATE,                  // int32 类型，bps 单位，|int32 type, bps unit
    EOP_DOWNLOAD_STATE,                    //EOP_DOWNLOAD_STATE_OPT_E 指针类型，如上，0 下载开始,1 下载暂停,2 下载等待,3 下载完成,4 下载失败 ,其他为非法值|EOP_DOWNLOAD_STATE_OPT_E pointer type, as above, 0 download start, 1 download pause, 2 download waiting, 3 download complete, 4 download failed, other values ​​are illegal
    EOP_DOWNLOAD_POST_INFO,                //char* 输入，海报的高度和地址，用冒号分割字符串"高度：url",如"200:http://10.10.10.10/10.jpeg"|char* input, the height and address of the poster, separate the string "height: url" with a colon, such as "200:http://10.10.10.10/10.jpeg"
    EOP_DOWNLOAD_POST_LOCAL_URL,           //获取海报的本地地址,用冒号分割字符串"高度：content_id",如“200:22323-23-23-22-33”，传入string*|Get the local address of the poster, separate the string "height: content_id" with a colon, such as "200:22323-23-23-22-33", and input string*
    EOP_DOWNLOAD_PARALLEL_NUM,             //int32 类型输入,同时可以下载的任务数|Int32 type input, the number of tasks that can be downloaded at the same time
    EOP_DOWNLOAD_PRIORITY_LEVEL,           //char* 输入，,用冒号分割字符串"优先级：content_id",如“3:22323-23-23-22-33”,如从获取到的下载列表中，获取的顺序就是默认的优先级排序，排序为从1开始的整形，如1,2，3,...|char* input, split the string "priority: content_id" with a colon, such as "3:22323-23-23-22-33", such as from the download list obtained, the order of obtaining is the default priority order , The sorting is the integer starting from 1, such as 1, 2, 3,...
    EOP_DOWNLOAD_CA_MODE,                  //int32类型输入，0-在线，1-离线，默认在线认证方式|int32 type input, 0-online, 1-offline, the default online authentication mode

    EOP_DOWNLOAD_CHUNKS_RECEIVED,          //输出string，json串格式，{"bitrate":int}组合，{"ChunksReceived":{"10000":1,"20000":2}}
    EOP_DOWNLOAD_CHUNKS_ABORTED,           //输出string，json串格式，{"bitrate":int}组合，{"ChunksAborted":{"10000":1,"20000":2}}
    EOP_DOWNLOAD_CHUNKS_NON_AVALIABLE,     //输出string，json串格式，{"bitrate":int}组合，{"ChunkNonavailable":{"10000":1,"20000":2}}
    EOP_DOWNLOAD_CHUNKS_EXPECT,            //输出string，json串格式，{"bitrate":int}组合，{"ChunksExpected":{"10000":1,"20000":2}}
    EOP_DOWNLOAD_UNIQUE_DOWNLOAD,          //输出INT32, 周期内新建下载任务数
    EOP_DOWNLOAD_SUCCESS,                  //输出INT32，周期内下载成功任务数，包含被用户暂停但下载过程中未出错的任务
    EOP_DOWNLOAD_ABORTED,                  //输出INT32，周期内下载中断的任务数，如ts下载失败导致后续无法下载|Output INT32, the number of download interrupted tasks in the cycle, such as ts download failure and subsequent download failure
    EOP_DOWNLOAD_NON_AVALIABLE,            //输出INT32，周期内无法下载任务数，如index.m3u8下载失败|Output INT32, the number of tasks that cannot be downloaded in the cycle, such as index.m3u8 download failed
    EOP_DOWNLOAD_IN_COMPLETE,              //输出INT32，正在下载任务数，瞬时值|Output INT32, the number of downloading tasks, instantaneous value
    EOP_DOWNLOAD_SOURCE,                   //输出string，最后一个分片下载ip|Output string, download ip of the last fragment
    EOP_DOWNLOAD_AVG_SPEED,                //输出INT32， 周期内平均下载速率，kbps|Output INT32, average download rate in the cycle, kbps

    EOP_DOWNLOAD_CA_INFO,                  //char*输入，json格式，格式："CaInfo":{"ServerUrl":"http://http://10.137.13.100/playready.asmx","HttpHeaderData" : "xxxxxxxxxxxx","contentId":"11-22-33-44"}
    EOP_DOWNLOAD_LAST_STATE,               //同EOP_DOWNLOAD_STATE
    EOP_DOWNLOAD_SSL_VERIFY_PATH,          //char*输入，本地ssh证书所在文件夹路径
    EOP_DOWNLOAD_SWITCH_IPV6,              //输入INT32, 下载IPV6开关, 0 - 关闭，1 - 打开, 默认打开

    //101-200    本地服务器模块使用
	EOP_LOCAL_SERVER_STAT = 101,            //暂时不用，保留
	EOP_LOCAL_SERVER_PORT,                 //暂时不用，保留
	EOP_LOCAL_SERVER_IP,                   //暂时不用，保留
    EOP_LOCAL_SERVER_PLAY_URL,            //暂时不用，保留
    //201-300    本地存储使用
	EOP_SAVING_PATH = 201,     //设置保存路径
	EOP_SAVING_FREE_STORAGE, // INT64位 剩余空间,单位为字节
    EOP_SAVING_CONFIG,       //如果是获取的话，传入string* ，保存设置获取读取保存的设置，设置的时候传入的内容为用冒号分割字符串"content_id:config_name:config_content",获取时候传入参数格式为"content_id:config_name"
    EOP_ACTIVE_PATH_PARTITION_SIZE,  // INT64位，激活路径磁盘总空间，单位为字节
    EOP_ACTIVE_PATH_CONTENT_SIZE,    // INT64位，char*输入contentid，查询激活路径下任务所占空间，单位为字节
    EOP_FREESPACE_WARNING_THRESHOLD, // INT32位，剩余空间警告阈值，默认64M，单位为字节
    EOP_SPEC_PATH_FREE_STORAGE,      // INT64位，获取指定路径剩余空间，单位为字节， char*输入路径
    EOP_DOWNLOAD_TASK_PATH,          // 输入conentID,char*, 输出 path string*
	//301-400 通用码
	EOP_CALL_BACK = 301,    //一个函数指针设置选项，回调函数结构在EopPubdefine.h中定义的EOPCALLBACKCPP类型
	EOP_EVENT_JSON = 302,   //用户android主动获取事件码,返回的是json格式的事件格式,如下面描述的错误码

    //1000-...  保留使用



}EOP_OPT;

/*
*错误码描述|Error code description
错误场景|Error scene	错误码|Error code 错误信息|Error message 错误原因|wrong reason
本地下载|local download     301     片源网络不可用|Source network is unavailable
                            201	   下载ts失败|Download ts failed
                            202	   下载playlist失败（用户选择的码率）|Failed to download playlist (bitrate selected by user)
本地存储|Local storage	200    写文件失败|Failed to write file
                        401	   剩余空间不足|Not enough free space
                        402	   存储路径找不到|Storage path not found
                        403	   配置项读取失败|Configuration item failed to read
                        404	   流媒体协议解析失败|Streaming media protocol analysis failed
本地服务器|Local server	405	   发送给播放器数据失败|Failed to send data to the player
                        406	   读取流媒体文件失败|Failed to read streaming media file
                        407	   流媒体服务器初始化失败|Streaming media server failed to initialize

内容管理|Content management：
         状态类型|State type：
         101          下载开始|DOwnload begins
         102          下载暂停|Download paused
         103          下载等待|Download waiting
         104          下载完成|Download complete
         105          下载失败|Download failed


级别|Level	提示码段|Prompt Code	 解释|Explaination
INFO	    100-199	            非错误|Not wrong
WARNING	    200-299	            可自动恢复下载播放的错误|Can automatically resume download and playback errors
ERROR	    300-399	            可手工恢复下载播放的错误|Manually recover download and playback errors
FATAL	    400-499	            无法恢复的下载播放错误|Unrecoverable download playback error


事件码上报都是封装成json格式，如下: |Event code reports are all encapsulated in json format, as follows:
{
  "EventId": 210,
  "EventLevel":1,
  "EventContentId":"20923jaldkjf",
  "EventInfo": "写文件失败",
  "EventSource":"EOP OSM",
  "EventReason": "文件系统被删除"
}


*/

//定义下时间上报的回调结构|Define the callback structure for time reporting
typedef enum EOP_EVENT_CODE
{
  //100 - 199
  EOP_DOWNLOAD_STATUS_START = 101,
  EOP_DOWNLOAD_STATUS_PAUSE = 102,
  EOP_DOWNLOAD_STATUS_WAITING = 103,
  EOP_DOWNLOAD_STATUS_FINISHED = 104,
  EOP_DOWNLOAD_STATUS_FAILED = 105,

  EOP_DOWNLOAD_POST_FINISH = 110,

  //200 - 299
  EOP_FILE_WRITE_ERROR = 200,
  EOP_TS_DOWNLOAD_FAILED = 201,
  EOP_PLAYLIST_DOWNLOAD_FAILED = 202,

  //300 - 399
  EOP_SOURCE_NONAVAILABLE = 301,
  EOP_OSM_FILE_NO_EXIST = 302,
  EOP_ODM_CHECK_FILE_FAIL = 303,

  //400 - 499
  EOP_NO_FREE_STORAGE = 401,
  EOP_NOT_FONND_PATH = 402,
  EOP_CONFIG_READED_FAILED = 403,
  EOP_PROTOCOL_PARSE_FAILED = 404,
  EOP_SEND_DATA_FAILED = 405,
  EOP_READ_DATA_FAILED = 406,
  EOP_LOCAL_SERVER_INIT_FAILED = 407,
  EOP_DOWNLOAD_CA_FAILED = 408,

  //200001 - 200999
  EOP_LICENSE_LIMITED = 200001,     // US-20170819101518-2031026273， 证书受限，EOP功能无法使用|The certificate is restricted and the EOP function cannot be used

  //201001 - 201999
  EOP_NO_SSL_CERTIFICATE = 201001,  // US-20170819101826-2031026280, SSL证书未设置|SSL certificate is not set
  EOP_INVALID_SERVER = 201002,      //  US-20170819101826-2031026280, 校验服务器SSL证书失败|Failed to verify server SSL certificate
}EOP_EVENT_CODE_E;

typedef void (*EOPCALLBACKCPP)(EOP_EVENT_CODE_E event_id, void* paramter,void* user_data); //第三个参数代表是上层传递下来的一个自定义函数，用于回传回去|The third parameter represents a custom function passed down by the upper layer, which is used to pass back.

typedef struct EOP_CALL_BACK
{
	EOPCALLBACKCPP call_back;
	VOID* user_data;
}EOP_CALL_BACK_S,*EOP_CALL_BACK_P;
#endif
