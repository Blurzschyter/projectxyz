//
//  SQMDataPick.h
//  OTTSQM
//
//  Created by OTT on 2017/6/13.
//  Copyright © 2017年 DHOTT. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @class SQMMosInfo
 @brief Mos信息，包括卡顿次数、卡顿时长及uvmos得分
 */
@interface SQMMosInfo : NSObject
/**
 播放时延
 */
@property(nonatomic, assign) long playoutDelay;
/**
 卡顿次数
 */
@property(nonatomic, assign) long stallingCount;

/**
 卡顿次数，单位：ms
 */
@property(nonatomic, assign) long stallingDuration;

/**
 uvMos值
 */
@property(nonatomic, assign) float uvMos;
@end


/**
 SQM数据采集协议
 */
@protocol SQMDataPick <NSObject>

/**
 @brief 获取mos信息
 @return 返回mos信息
 */
- (SQMMosInfo *)getMosInfo;
@end

/**
 SQM数据采集类
 */
@interface SQMDataPick : NSObject<SQMDataPick>
@end
