//
//  OTTSQM.h
//  OTTSQM
//
//  Created by OTT on 2017/5/15.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SQM_SDK_VERSION @"20.0.21.23"
#define SQM_SDK_REQUIRED_DMP_VERSION @"DmpPlayer_20.0.21.23"

/**
 The version of SQM SDK.
 */
extern const NSString *const SQMSDK_Version;

/**
 The version of DmpPlayer that SQM SDK is based on.
 Player with older version is not supported.
 Future versions of player are supposed to be compatible with this SDK.
 You call GetDmpBaseVer() to fetch version of DmpPlayer.
 */
extern const NSString *const SQMSDK_Supporting_Dmpplayer_Version;
