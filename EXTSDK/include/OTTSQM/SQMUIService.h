//
//  SQMUIService.h
//  OTTSQM
//
//  Created by OTT on 2017/5/19.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

// 网络状态
typedef NS_ENUM(NSInteger, SQMNetworkStatus) {
    // Apple NetworkStatus Compatible Names.
    SQMNotReachable = 0,
    SQMReachableViaWiFi = 2,
    SQMReachableViaWWAN = 1
};

/**
 @class SQMRequestError
 @brief Request error，主要是网络数据请求
 */
@interface SQMRequestError : NSObject
/**
 @brief 请求的name
 */
@property(nonatomic, strong) NSString *name;

/**
 @brief 请求的url
 */
@property(nonatomic, strong) NSString *url;

/**
 @brief 请求的错误信息
 */
@property(nonatomic, strong) NSString *errorMessage;

/**
 @brief 请求错误码
 */
@property(nonatomic, strong) NSString *errorCode;

/**
 If receiver is EPG request error. YES by default.
 */
@property (nonatomic) BOOL isEPGError;

/**
 If receiver is login request error. NO by default.
 */
@property (nonatomic) BOOL isLoginError;

@end

/**
 @class SQMNetworkChange
 @brief 网络切换
 */
@interface SQMNetworkChange : NSObject
@property (nonatomic, assign) SQMNetworkStatus networkStatus;
@end

/**
 @class Scenario
 @brief 页面数据统计场景
 */
@interface Scenario : NSObject
extern NSString *const ScenarioKeyBootTime;
extern NSString *const ScenarioKeyLoginCounter;
extern NSString *const ScenarioKeyNetworkChange;
extern NSString *const ScenarioKeyChannelList;
extern NSString *const ScenarioKeyRecommendation;
extern NSString *const ScenarioKeyRecording;
extern NSString *const ScenarioKeySearch;
extern NSString *const ScenarioKeyHomePage;
extern NSString *const ScenarioKeyTVGuide;
extern NSString *const ScenarioKeyVODList;
extern NSString *const ScenarioKeyVODDetail;
extern NSString *const ScenarioKeyEDS;
extern NSString *const ScenarioKeyLogin;
extern NSString *const ScenarioKeyAuth;
extern NSString *const ScenarioKeyPlayAuthorize;

/**
 @brief 页面场景的key
 @discussion
 主要涉及的key：
 == Initialization ==
 1. BootTime         启动时长，单位：秒。从App启动到首页展示完成
 2. LoginCounter     登陆
 3. NetworkChange    网络切换次数
 
 == Function ==
 1. ChannelList     获取频道列表
 2. Recommendation  Recommendation 操作
 3. Recording       Recording 操作
 4. Search          Search 操作
 
 == EPG ==
 1. HomePage    EPG首页请求
 2. TVGuide     TVGuide页面请求总次数
 3. VODList     VOD列表页面
 4. VODDetail   VOD详情页面响应成功次数，即除4xx/5xx和20无响应的次数
 5. EDS         APP调用EDS的重定向的时延，单位：毫秒，超时填-1。
 6. Login       APP调用Login接口的时延，单位：毫秒。如果有多次，以逗号分隔，例如：300,400,-1
 7. Auth        APP调用Authenticate接口的时延，单位：毫秒。如果有多次，以逗号分隔，例如：300,400,-1
 
 == Play Authorize ==
 1. PlayAuthorize 播放鉴权
 */
@property (nonatomic, strong) NSString *key;

/**
 @brief 开始时间，距1970年毫秒数
 @discussion 场景开始时间，距1970年毫秒数
 */
@property (nonatomic, assign) long long startTime;

/**
 @brief 场景结束时间，距1970年毫秒数，[NSNumber numberWithLong:]
 */
@property (nonatomic, assign) long long endTime;

/**
 @brief 是否成功
 */
@property (nonatomic, assign) BOOL isSuccess;

/**
 @brief 操作的结果code
 */
@property (nonatomic, strong) NSString *resultCode;
@end

/**
 @protocol SQMUIService
 @brief UI服务，在SQMService中返回对象，应用调用，主要实现收集页面场景的数据，包括网络请求，网络切换，页面场景统计等
 */
@protocol SQMUIService <NSObject>

/**
 @brief 网络请求错误统计
 @param requestError 网络请求错误
 */
- (void)onRequestError:(SQMRequestError *)requestError;

/**
 @brief 网络切换
 @param networkChange 网络切换
 */
- (void)onNetworkChange:(SQMNetworkChange *)networkChange;

/**
 @brief 页面场景
 @param scenario 页面场景
 */
- (void)recordedAppScenario:(Scenario *)scenario;

/**
 @brief 接口性能
 @param jsondata 接口性能
 */
- (void)onReportInterface:(NSString *)jsondata;
@end
