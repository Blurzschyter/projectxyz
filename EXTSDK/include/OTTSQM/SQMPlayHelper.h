//
//  SQMPlayHelper.h
//  OTTSQM
//
//  Created by OTT on 2017/5/17.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SQMServiceType) {
    SQMPlayTypeLiveTv = 0,      // LiveTV
    SQMPlayTypeVod = 1,         // VOD
    SQMPlayTypeRecording = 2,   // Recording
    SQMPlayTypeUnKown = 3,      // Unkown
};

// ServiceType
extern NSString *const ServiceTypeLiveTV;
extern NSString *const ServiceTypeVod;
extern NSString *const ServiceTypeRecording;
extern NSString *const ServiceTypeIR;
extern NSString *const ServiceTypeNVOD;

/**
 @protocol SQMPlayHelper
 @brief 主要实现player相关数据获取的接口
 */
@protocol SQMPlayHelper<NSObject>

/**
 @brief 获取播放内容的ID
 @return 播放内容ID
 */
- (NSString *)getContentID;

/**
 @brief 业务类型，如LiveTV，VOD，Recording，IR，NVOD
 @return 业务类型，如LiveTV，VOD，Recording，IR，NVOD
 */
- (NSString *)getServiceType;

/**
 @brief 获取播放的url，如http://xxx.xxx.xxx.xxx:port/xxx/
 @return 播放的url
 */
- (NSString *)getPlayUrl;

/**
 @brief 获取当前播放的时间点，单位：毫秒
 @return 获取当前播放的时间点，单位：毫秒
 */
- (NSInteger)getCurrentPosition;

/**
 @brief 对于时移频道，当前播放直播，则返回true，时移状态返回false
 @return 对于时移频道，当前播放直播，则返回true，时移状态返回false
 */
- (BOOL)isPlayingBTV;

/**
 @brief 获取视频的帧率，默认： [NSNumber numberWithInt:]
 @return 获取视频的帧率，默认： [NSNumber numberWithInt:]
 */
- (NSNumber *)getVideoFrameRate;

/**
 @brief 获取当前播放内容的帧率，默认：30 [NSNumber numberWithInt:]
 @return 获取当前播放内容的帧率，默认：30 [NSNumber numberWithInt:]
 */
- (NSNumber *)getFrameRate;

/**
 @brief 获取媒体编码类型，如H264,H265等
 @return 获取媒体编码类型，如H264,H265等
 */
- (NSString *)getVideoCodec;

/**
 @brief 设备尺寸，如iPad air，9.7英寸；iPhone 7，4.7英寸；[NSNumber numberWithDouble:]
 @return 设备尺寸，如iPad air，9.7英寸；iPhone 7，4.7英寸；[NSNumber numberWithDouble:]
 */
- (NSNumber *)getScreenSize;

/**
 @brief 代表视频内容采用开源ffmpeg编码
 @return 代表视频内容采用开源ffmpeg编码
 */
- (NSNumber *)getContentProvider;

/**
 @brief 视频分辨率高度
 @return 视频分辨率高度
 */
- (NSNumber *)getVideoResolutionHeight;

/**
 @brief 视频分辨率宽度
 @return 视频分辨率宽度
 */
- (NSNumber *)getVideoResolutionWidth;

/**
 @brief 获取下载速度
 @return 获取下载速度，返回值，[NSNumber numberWithLongLong] 单位，bps
 */
- (NSNumber *)getDownloadSpeed;

/**
 @brief 获取已下载大小,单位：字节（byte）[NSNumber numberWithLongLong]
 @return  获取已下载大小,单位：字节（byte）[NSNumber numberWithLongLong]
 */
- (NSNumber *)getDownloadSize;

/**
 @brief 获取码率列表
 @return  获取码率列表
 */
- (NSArray<NSString *> *)getMediaBitrates;

/**
 @brief 获取直播时播放器的播放绝对时间
 @return  获取直播时播放器的播放绝对时间, 单位:秒  [NSNumber numberWithLong:]
 */
- (NSNumber *)getLiveAbosoluteTime;

/**
 @brief 查询ios播放器缓存长度,单位为秒
 @return  查询ios播放器缓存长度,单位为秒 ，单位为秒,[NSNumber numberWithLong:]
 */
- (NSNumber *)getBufferedLength;

/**
 @brief 查询播放器状态
 @return  查询查询播放器状态 ，[NSNumber numberWithInt],DmpPlayer_state类型
 @discussion 主要状态：
 0:idle state,Player has created;
 1:init state,Player has been set with media source
 2:preparing state,Player has been started and preparing for media data
 3:buffering state,Player is receiving data
 4:playing state,Playback is in progress
 5:playing state,Playback is paused
 6:play end state,Playback is finished
 7:stop state,Player has been stopped
 8:error state,Error occurs
 9:unknown state
 */
- (NSNumber *)getPlayerState;

/**
 @brief 查询当前码率
 @return  查询当前码率 ，返回值: [NSNumber numberWithInteger]
 */
- (NSNumber *)getPlayerBandwidth;

/**
 @brief 获取正片时长
 @return 返回正片时长 ，返回值: [NSNumber numberWithInteger]
 */
- (NSNumber *)getDuration;

/**
 @brief 获取视频播放进度
 @return 返回播放进度
 */
- (NSNumber *)getProgress;

/**
 @brief 获取视频的帧率
 @return 返回视频的帧率 ，返回值: [NSNumber numberWithDouble] eg：25,表示一秒钟25帧
 */
- (NSNumber *)getVideoInfoFPS;

/**
 @brief 获取视频的当前帧率
 @return 返回视频的当前帧率 ，返回值: [NSNumber numberWithDouble] eg：25,表示当前帧率一秒钟25帧
 */
- (NSNumber *)getVideoCurrentFPS;

/**
 @brief 获取视频分辨率的最大宽度
 @return 返回视频分辨率的最大宽度 ，返回值: [NSNumber numberWithInteger] 单位：像素点 eg:1920
 */
- (NSNumber *)getVideoMaxWidth;

/**
 @brief 获取视频分辨率的最大高度
 @return 返回视频分辨率的最大高度 ，返回值: [NSNumber numberWithInteger] 单位：像素点 eg:1080
 */
- (NSNumber *)getVideoMaxHeight;

/**
 @brief 获取视频的高度
 @return 返回视频的高度 ，返回值: [NSNumber numberWithInteger] 单位：像素点  eg:480
 */
- (NSNumber *)getVideoHeight;

/**
 @brief 获取视频的宽度
 @return 返回视频的宽度 ，返回值: [NSNumber numberWithInteger] 单位：像素点  eg:320
 */
- (NSNumber *)getVideoWidth;

/**
 @brief 获取实时码率
 @return 返回实时码率，返回值: [NSNumber numberWithInteger]
 */
- (NSNumber *)getPlayRealtimeBitrate;

/**
 @brief 获取最后出错地址（域名/IP地址）,NSString
 @return 获取最后出错地址（域名/IP地址）,NSString
 */
- (NSString *)getLastErrorHost;

/**
 @brief 连接建立的时间点查询，即startUpEvent中最后一个事件的时间点,[NSNumber numberWithLongLong:]
 @return 连接建立的时间点查询，即startUpEvent中最后一个事件的时间点, 返回值: [NSNumber numberWithLongLong:]
 */
- (NSNumber *)getAccessDelay;

/**
 @brief 【本节目的】的卡顿记录，格式：时间点，卡顿时长 ;时间点为相对StartTime的秒数，卡顿时长单位为毫秒，小于200毫秒的卡顿不记录。多次卡顿分号分隔，最多记录20次。5,2543;12,1528;312,8457
 @return 【本节目的】的卡顿记录
 */
- (NSString *)getStallingTrack;

/**
 @brief 分片请求总数
 @return 分片请求总数, 返回值:[NSNumber numberWithInt:]
 */
- (NSNumber *)getNRofSegmentsRequested;

/**
 @brief 分片已接受总数
 @return 分片已接受总数, 返回值:[NSNumber numberWithInt:]
 */
- (NSNumber *)getNRofSegmentsReceived;

/**
 @brief 用户选择的清晰度，Auto，SD，HD，4K，或者Auto，480P，720P，1080P
 @return 返回用户选择的清晰度
 */
- (NSString *)getVideoQualitySetByUser;

/**
 获取本次播放唯一对应的SessionID，该SessionID用于唯一标识本次播放
 
 @return 本次播放唯一对应的SessionID
 */
- (NSString *)getPlaybackID;

@end

@interface SQMPlayHelper : NSObject<SQMPlayHelper>
@end
