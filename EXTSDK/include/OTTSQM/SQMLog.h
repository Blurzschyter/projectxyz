//
//  SQMLog.h
//  OTTSQM
//
//  Created by OTT on 2017/5/17.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @protocol SQMLog
 @brief 日志打印协议，需要实现debug、info、error等日志级别打印
 */
@protocol SQMLog <NSObject>
/**
 @brief debug日志级别打印
 @param tag 日志标识，比如app
 @param logmsg 日志信息
 */
- (void)debug:(NSString *)tag logmsg:(NSString *)logmsg;

/**
 @brief info日志级别打印
 @param tag 日志标识，比如app
 @param logmsg 日志信息
 */
- (void)info:(NSString *)tag logmsg:(NSString *)logmsg;

/**
 @brief error日志级别打印
 @param tag 日志标识，比如app
 @param logmsg 日志信息
 */
- (void)error:(NSString *)tag logmsg:(NSString *)logmsg;

@end

@interface SQMLog : NSObject<SQMLog>
@end
