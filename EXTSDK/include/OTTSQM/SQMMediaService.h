//
//  SQMMediaService.h
//  OTTSQM
//
//  Created by OTT on 2017/5/18.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @protocol SQMMediaService
 @brief 媒体服务，对象在SQMService返回，应用调用相关方法，实现媒体播放数据收集
 */
@protocol SQMMediaService <NSObject>

/**
 @brief open方法，在媒体open式调用，即player创建后调用
 */
- (void)onOpen;

/**
 @brief load方法，在媒体加载完成时调用
 */
-(void) onLoad;

/**
 @brief buffer方法，媒体缓冲时调用，缓冲状态和非缓冲状态之间转换时触发
 @param buffering 缓冲时为TRUE，缓冲结束时为FALSE
 */
-(void) onBuffering:(BOOL)buffering;

/**
 @brief seek方法，seek开始时调用此方法
 */
-(void) onSeekStart;

/**
 @brief seek完成时调用此方法
 */
-(void) onSeekComplete;

/**
 @brief 进度改变时调用此方法
 @discussion
 当播放进度改变时触发，progress 播放进度，单位为s
 对于vod节目,返回和设置秒数(正数),
 对于时移,返回和设置相对直播时间的秒数(负数),当前时间即直播时间为原点0,对纯直播无效(返回nan).
 如果获取进度失败也会返回nan
 @param progress 当前播放进度，单位：s
 */
-(void) onProgress:(CFTimeInterval)progress;

/**
 @brief 节目正常播放完成时调用此方法
 */
-(void) onComplete;

/**
 @brief 节目被关闭或替换时触发
 */
-(void) onUnload;

/**
 @brief 从暂停状态到播放状态
 */
-(void) onPlay;

/**
 @brief 从播放状态到暂停状态
 */
-(void) onPause;

/**
 @brief 播放发生错误时上报
 @param errorName 错误码
 @param errorType 错误类型
 */
-(void) onError:(NSString*)errorName errorType:(int)errorType;

/**
 @brief 时间上报，目前只支持卡顿时间上报
 @param eventCode 事件码
 @param info 卡顿时长，单位：ms
 */
-(void) onSqmEvent:(NSString*)eventCode eventInfo:(id)info;

/**
 @brief 码率切换时调用方法
 @param bitrate 码率，单位：bps
 */
-(void)onBitrateSwitch:(NSNumber *)bitrate;

/**
 @brief 播放释放之前的事件
 */
-(void)beforeRelease;
@end
